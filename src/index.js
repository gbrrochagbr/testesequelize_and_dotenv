const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const sequelize = new Sequelize("teste123", "root", null, {
  host: "localhost",
  dialect: "mysql"
});

sequelize
  .authenticate()
  .then(function() {
    console.log("Conectou ao banco de boas");
  })
  .catch(err => {
    console.log("falha ao conectar ao banco", err);
  });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.listen(process.env.PORT);
